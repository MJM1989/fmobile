import { BehaviorSubject } from 'rxjs/BehaviorSubject';

export function Theme () {
  const $themeChanged = new BehaviorSubject(getTheme());

  function getTheme () {
    return localStorage.getItem('theme');
  }

  function setTheme (theme) {
    document.getElementById('theme-icon').setAttribute('href', '/icons/fav-icons/favicon-' + theme + '.ico');
    document.getElementById('apple-icon-120').setAttribute('href', '/icons/touch-icons/apple-touch-icon-120-' + theme + '.png');
    document.getElementById('apple-icon-152').setAttribute('href', '/icons/touch-icons/apple-touch-icon-152-' + theme + '.png');
    document.getElementById('apple-icon-167').setAttribute('href', '/icons/touch-icons/apple-touch-icon-167-' + theme + '.png');
    document.getElementById('apple-icon-180').setAttribute('href', '/icons/touch-icons/apple-touch-icon-180-' + theme + '.png');
    document.getElementById('theme-color').setAttribute('href', 'https://www.w3schools.com/lib/w3-theme-' + theme + '.css');
    $themeChanged.next(theme);
    localStorage.setItem('theme', theme);
  }

  return {
    getTheme,
    setTheme,
    $themeChanged
  };
}
